from timeit import default_timer as timer

def partition(A, p, r):
    x = A[r]  # element wyznaczajacy podział
    i = p - 1
    for j in range(p, r + 1):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i - 1


c = 5


def quickSort(A, p=None, r=None):
    if p is None:
        p = 0
    if r is None:
        r = len(A) - 1

    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q)
        quickSort(A, q + 1, r)


def quickSortIns(A, p=None, r=None):
    if p is None:
        p = 0
    if r is None:
        r = len(A) - 1

    if p < r:
        if r - p + 1 < c:
            for i in range(p, r + 1):
                for j in range(p, r + 1):
                    if A[i] < A[j]:
                        A[i], A[j] = A[j], A[i]
        else:
            q = partition(A, p, r)
            quickSort(A, p, q)
            quickSort(A, q + 1, r)


tests = [
    [28, 6, 11, 12, 7, 5, 14, 2, 23],
    [1, 2, 3, 4, 5, 6, 7, 8, 9],
    [30, 27, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1],
]

for test in tests:
    A = test.copy()
    B = test.copy()
    print(A)
    print("quickSort", end=' - ')
    start = timer()
    quickSort(A)
    end = timer()
    print(f'{end - start:.10f}'.rstrip('0'))
    print("quickSortIns", end=' - ')
    start = timer()
    quickSortIns(B)
    end = timer()
    print(f'{end - start:.10f}'.rstrip('0'))
    print()

# [28, 6, 11, 12, 7, 5, 14, 2, 23]
# quickSort - 0.000012194
# quickSortIns - 0.00000555
#
# [1, 2, 3, 4, 5, 6, 7, 8, 9]
# quickSort - 0.000004227
# quickSortIns - 0.000004018
#
# [30, 27, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1]
# quickSort - 0.000005586
# quickSortIns - 0.000005498
