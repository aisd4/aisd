

class BinaryTree:

    def __init__(self, value, level=0):
        self.left: BinaryTree | None = None
        self.right: BinaryTree | None = None
        self.value = ord(value)
        self.level = level
        self.count = 1

    def __str__(self):
        return f"{chr(self.value)} ilosc: {self.count} poziom: {self.level}"

    def drukuj(self):
        if self.left is not None:
            self.left.drukuj()
        print(chr(self.value), 'ilosc:', self.count, 'poziom:', self.level)
        if self.right is not None:
            self.right.drukuj()

    def wstaw(self, value):
        value = BinaryTree(value, self.level + 1)
        if self.value == value.value:
            self.count += 1
        elif value.value < self.value:
            if self.left is None:
                self.left = value
            else:
                self.left.wstaw(chr(value.value))
        else:
            if self.right is None:
                self.right = value
            else:
                self.right.wstaw(chr(value.value))

    def szukaj(self, value):
        if self.value == ord(value):
            return self
        elif self.value < ord(value):
            if self.right is None:
                return None
            else:
                return self.right.szukaj(value)
        else:
            if self.left is None:
                return None
            else:
                return self.left.szukaj(value)

    def usun(self, value):
        value = ord(value)
        if self.value == value:
            self.left = None
            self.right = None
        elif self.value > value:
            if self.right is not None:
                if self.right.value == value:
                    self.right.count -= 1
                    if self.right.count == 0:
                        self.right = None
                else:
                    self.right.usun(chr(value))
        else:
            if self.left is not None:
                if self.left.value == value:
                    self.left.count -= 1
                    if self.left.count == 0:
                        self.left = None
                else:
                    self.left.usun(chr(value))

def main():
    root = BinaryTree('L')
    #               L
    #             /   \
    #            D     P
    #           / \     \
    #          A   E     X
    #           \       /
    #            C     T
    root.wstaw('D')
    root.wstaw('P')
    root.wstaw('A')
    root.wstaw('C')
    root.wstaw('X')
    root.wstaw('T')
    root.wstaw('E')
    root.drukuj()
    print()
    print(root.szukaj('C'))
    print()
    root.usun('D')
    root.drukuj()
    print()
    print('C: ', root.szukaj('C'))
    print()
    root.wstaw('T')
    print(root.szukaj('T'))
    root.usun('T')
    print(root.szukaj('T'))
    root.usun('T')
    print(root.szukaj('T'))

if __name__ == '__main__':
    main()