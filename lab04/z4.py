# ZADANIE 4.1

class LinkedList:
    def __init__(self):
        self.value = None
        self.next: LinkedList | None = None

    def wstaw(self, s):
        if self.value is None:
            self.value = s
            return
        else:
            if self.next is None:
                self.next = LinkedList()
            self.next.wstaw(s)

    def drukuj(self):
        print(self.value)
        if self.next is not None:
            self.next.drukuj()

    def szukaj(self, s):
        if self.value == s:
            return self
        else:
            if self.next is None:
                return None
            else:
                return self.next.szukaj(s)

    def usun(self, s):
        if self.value == s:
            self.value = self.next.value
            self.next = self.next.next
        else:
            if self.next is not None:
                self.next.usun(s)

    def klonuj(self):
        cpy = LinkedList()
        cpy.value = self.value
        if self.next is not None:
            cpy.next = self.next.klonuj()
        return cpy

    def bezpowtorzen(self):
        cpy = self.klonuj()
        if cpy.next is not None:
            cpy.next = self.next.bezpowtorzen()
            if cpy.next.value == cpy.value:
                cpy.next = cpy.next.next
        return cpy

    def scal(self, lst):
        if self.next is None:
            self.next = lst
        else:
            self.next.scal(lst)

        return self

    def dlugosc(self):
        suma = 1
        if self.next is not None:
            suma += self.next.dlugosc()
        return suma


def main():
    lst = LinkedList()
    lst.wstaw("a")
    lst.wstaw("b")
    lst.wstaw("c")
    lst.wstaw("c")
    lst.wstaw("d")
    lst.drukuj()

    print("\nszukaj 'b' w liscie elementow [ 'a', 'b', 'c', 'c', 'd' ]")
    print(lst.szukaj("b").value)

    print("\nusun element 'b'")
    lst.usun("b")
    lst.drukuj()

    print('\nbezpowtorzen')
    lst.bezpowtorzen().drukuj()
    print('\nporownanie')
    lst.drukuj()

    print("\nscal [ 'a', 'b', 'c' ] i [ 'd', 'e', 'f' ]")
    ls1 = LinkedList()
    ls1.wstaw("a")
    ls1.wstaw("b")
    ls1.wstaw("c")
    ls1.drukuj()
    print()
    ls2 = LinkedList()
    ls2.wstaw("d")
    ls2.wstaw("e")
    ls2.wstaw("f")
    ls2.drukuj()
    print()
    ls3 = ls1.scal(ls2)
    ls3.drukuj()

    print('dlugosc:', ls3.dlugosc())


if __name__ == "__main__":
    main()
