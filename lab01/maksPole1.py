from timeit import default_timer as timer
from random import randint


# n^6
def NAIWNY(n, M):
    maks = 0
    lokalMaks = 0
    for x1 in range(0, n):
        for y1 in range(0, n):
            for x2 in range(n - 1, x1 - 1, -1):
                for y2 in range(n - 1, y1 - 1, -1):
                    lokalMaks = 0
                    for x in range(x1, x2 + 1):
                        for y in range(y1, y2 + 1):
                            lokalMaks += M[x][y]
                    if lokalMaks == (x2 - x1 + 1) * (y2 - y1 + 1) and lokalMaks > maks:
                        maks = lokalMaks
    return maks


# n^3
def DYNAMICZNY(n, M):
    maks = 0
    #  utworz tablice kwadratowe MM rozmiaru n
    #  i wypelnij ja zerami
    MM = [[0 for _ in range(n)] for _ in range(n)]
    for y in range(0, n):
        for x1 in range(0, n):
            iloczyn = 1
            for x2 in range(x1, n):
                iloczyn *= M[x2][y]
                MM[x1][x2] = iloczyn * (x2 - x1 + 1 + MM[x1][x2])
                if MM[x1][x2] > maks:
                    maks = MM[x1][x2]
    return maks


# n^3
def CZULY(n, M):
    maks = 0
    lokalMaks = 0

    for x1 in range(0, n):
        for y1 in range(0, n):
            lokalMaks = 0
            x2 = x1
            ymaks = n - 1
            while (x2 < n) and (M[x2][y1] == 1):
                y2 = y1
                while (y2 < ymaks + 1) and (M[x2][y2] == 1):
                    y2 += 1
                ymaks = y2 - 1
                lokalMaks = (x2 - x1 + 1) * (ymaks - y1 + 1)
                if lokalMaks > maks:
                    maks = lokalMaks
                x2 += 1
    return maks


ns = [10, 20, 30]
for n in ns:
    full = [[1 for _ in range(n)] for _ in range(n)]
    empty = [[0 for _ in range(n)] for _ in range(n)]
    rand = [[randint(0, 1) for _ in range(n)] for _ in range(n)]
    MS = [full, empty, rand]
    for M in MS:
        start = timer()
        NAIWNY(n, M)
        stop = timer()
        time = format(stop - start, '.10f').rstrip('0')
        print("NAIWNY", f"{n}x{n}", "czas:", time)

        start = timer()
        DYNAMICZNY(n, M)
        stop = timer()
        time = format(stop - start, '.10f').rstrip('0')
        print("DYNAMICZNY", f"{n}x{n}", "czas:", time)

        start = timer()
        CZULY(n, M)
        stop = timer()
        time = format(stop - start, '.10f').rstrip('0')
        print("CZULY", f"{n}x{n}", "czas:", time)
        print()

# NAIWNY
# pełna macierz
# 10x10 - 0.0060617
# 20x20 - 0.1885471
# 30x30 - 1.6434403

# pusta macierz
# 10x10 - 0.0056679
# 20x20 - 0.196615
# 30x30 - 1.5850522

# losowa macierz
# 10x10 - 0.0059385
# 20x20 - 0.2191422
# 30x30 - 1.6181476


# DYNAMICZNY
# pełna macierz
# 10x10 - 0.0001441
# 20x20 - 0.0007692
# 30x30 - 0.0025135

# pusta macierz
# 10x10 - 0.000146
# 20x20 - 0.000817
# 30x30 - 0.0027085

# losowa macierz
# 10x10 - 0.0001604
# 20x20 - 0.0010077
# 30x30 - 0.0025238


# CZULY
# pełna macierz
# 10x10 - 0.0003736
# 20x20 - 0.0036329
# 30x30 - 0.016848

# pusta macierz
# 10x10 - 0.0000091
# 20x20 - 0.0000439
# 30x30 - 0.0000759

# losowa macierz
# 10x10 - 0.0000446
# 20x20 - 0.0001595
# 30x30 - 0.0003136

