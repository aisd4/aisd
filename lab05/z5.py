from typing import List
from linkedlist import LinkedList

def get_mod_hash(m):
    def mod_hash(k):
        n = 0
        for char in k:
            n = n * 256 + ord(char)
        return n % m

    return mod_hash

def get_builtin_hash(m):
    return lambda x: hash(x) % m

def get_weak_hash(m):
    return lambda x: len(x) % m

def main(limit, print_first=False):
    ms = [17, 1031, 1024]
    hs = [get_mod_hash, get_builtin_hash, get_weak_hash]

    lines = [line.strip() for line in open('3700.txt')]
    if limit > 0:
        lines = lines[:limit]

    for hf in hs:
        for m in ms:
            h = hf(m)
            T: List[None | LinkedList] = [None for _ in range(m)]
            for line in lines:
                hsh = h(line)
                if T[hsh] is None:
                    T[hsh] = LinkedList()
                T[hsh].wstaw(line)

            print(f"m = {m}, hash = {hf.__name__.lstrip('get_').rstrip('_hash')}")
            for i in range(m):
                if T[i] is not None:
                    print(f"{i}: ", end="")
                    if print_first:
                        print(T[i].value, end="")
                        if T[i].dlugosc() > 1:
                            print(" len:", T[i].dlugosc(), end="")
                        print()
                    else:
                        T[i].drukuj()

            print()

            empty = m - len([x for x in T if x is not None])
            print(f"puste wyrazy: {empty}")
            mx = max([x.dlugosc() for x in T if x is not None])
            print(f"maksymalna dlugosc listy: {mx}")
            avg = sum([x.dlugosc() for x in T if x is not None]) / len([x for x in T if x is not None])
            print(f"srednia dlugosc niepustych list:", "%.2f" % avg)
            print()
            print()

if __name__ == '__main__':
    LIMIT = 5
    PRINT_FIRST = True
    main(LIMIT, PRINT_FIRST)






