# lab04/z4.py

class LinkedList:
    def __init__(self):
        self.value = None
        self.next: LinkedList | None = None

    def wstaw(self, s):
        if self.value is None:
            self.value = s
            return
        else:
            if self.next is None:
                self.next = LinkedList()
            self.next.wstaw(s)

    def drukuj(self):
        print(self.value)
        if self.next is not None:
            self.next.drukuj()

    def szukaj(self, s):
        if self.value == s:
            return self
        else:
            if self.next is None:
                return None
            else:
                return self.next.szukaj(s)

    def usun(self, s):
        if self.value == s:
            if self.next is not None:
                self.value = self.next.value
                self.next = self.next.next
            else:
                self.value = None
        else:
            if self.next is not None:
                self.next.usun(s)

    def klonuj(self):
        cpy = LinkedList()
        cpy.value = self.value
        if self.next is not None:
            cpy.next = self.next.klonuj()
        return cpy

    def bezpowtorzen(self):
        cpy = self.klonuj()
        if cpy.next is not None:
            cpy.next = self.next.bezpowtorzen()
            if cpy.next.value == cpy.value:
                cpy.next = cpy.next.next
        return cpy

    def scal(self, lst):
        if self.next is None:
            self.next = lst
        else:
            self.next.scal(lst)

        return self

    def dlugosc(self):
        suma = 1
        if self.next is not None:
            suma += self.next.dlugosc()
        return suma
