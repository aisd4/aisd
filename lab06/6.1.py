from typing import List

m = 13

def h1(k):
    return k % m

def h(k, i) -> int:
    return (h1(k) + i) % m

# wariant A
def main():
    Tl = [6, 19, 28, 41, 54]
    T: List = [None for _ in range(m)]
    for el in Tl:
        i = 0
        idx = h(el, i)
        while T[idx] is not None:
            print(f"kolizja dla {el} na pozycji {idx}")
            i += 1
            idx = h(el, i)
        T[idx] = el
        print(f"zapisano {el} na pozycji {idx}")
    for i in range(m):
        if T[i] is not None:
            print(f"{i}: {T[i]}")

    # zapisano 6 na pozycji 6
    # kolizja dla 19 na pozycji 6
    # zapisano 19 na pozycji 7
    # zapisano 28 na pozycji 2
    # kolizja dla 41 na pozycji 2
    # zapisano 41 na pozycji 3
    # kolizja dla 54 na pozycji 2
    # kolizja dla 54 na pozycji 3
    # zapisano 54 na pozycji 4

    # 2: 28
    # 3: 41
    # 4: 54
    # 6: 6
    # 7: 19

    print("usuwamy 41")
    i = 0
    idx = h(41, i)
    while T[idx] != 41:
        i += 1
        idx = h(41, i)
    T[idx] = "DEL"
    print(f"usunieto 41 z pozycji {idx}")
    for i in range(m):
        if T[i] is not None:
            print(f"{i}: {T[i]}")

    print("szukamy 54")
    i = 0
    idx = h(54, i)
    while T[idx] != 54:
        i += 1
        idx = h(54, i)
    print(f"znaleziono 54 na pozycji {idx}")

    print("szukamy 32")
    i = 0
    idx = h(32, i)
    while T[idx] != 32 and i < m:
        i += 1
        idx = h(32, i)
    if i == m:
        print("nie znaleziono 32")
    else:
        print(f"znaleziono 32 na pozycji {idx}")


if __name__ == "__main__":
    main()
