import random
from typing import List

def get_hash(m):
    def h1(k):
        return k % m

    def h2(k):
        return 1 + (k % (m - 2))

    def h_lin(k, i) -> int:
        return (h1(k) + i) % m

    def h_kwa(k, i) -> int:
        return (h1(k) + (i * h2(k))) % m

    def hsh(k, i):
        n = 0
        for char in k:
            n = n * 256 + ord(char)
        # return h_lin(n, i)
        return h_kwa(n, i)

    return hsh


def avg_from_list(l):
    s = 0
    for el in l:
        s += el
    return round(s / len(l), 2)

def test(m, nazwiska, mx):
    print('rozmiar tablicy:', m)
    h = get_hash(m)
    ilosci_operacji = {
        '50': [],
        '70': [],
        '90': []
    }
    Te: List = [None for _ in range(m)]

    def insert(T, tk):
        added = 0
        for nazwisko in nazwiska:
            if added > mx:
                break
            i = 0
            idx = h(nazwisko["nazwisko"], i)
            no_space = False
            while T[idx] is not None:
                i += 1
                idx = h(nazwisko["nazwisko"], i)
                if i > m:
                    no_space = True
                    break
            if not no_space:
                T[idx] = nazwisko
                added += 1
                ilosci_operacji[tk].append(i + 1)

    def test_filled(x):
        n = int(m * (x / 100))
        print("ilosc zapelnionych na start: ", n)
        print("ilosc wstawianych: ", m - n)
        for i in range(n):
            Te[i] = True
        random.shuffle(Te)
        insert(Te, str(x))
        for i in range(len(Te)):
            Te[i] = None
        ilosci_operacji[str(x)] = avg_from_list(ilosci_operacji[str(x)])



    test_filled(50)
    test_filled(70)
    test_filled(90)
    print(ilosci_operacji)
    print()


def real():
    pass

def main():
    nazwiska: List = [line.strip() for line in open('nazwiskaASCII.txt')]
    for i in range(len(nazwiska)):
        s = nazwiska[i].split()
        nazwiska[i] = {
            'ilosc': int(s[0]),
            'nazwisko': s[1]
        }
    print('ilosc nazwisk: ', len(nazwiska))
    print()
    test(37, nazwiska, 34)
    test(1987, nazwiska, 1800)

if __name__ == '__main__':
    main()